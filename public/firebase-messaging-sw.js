importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

var config = {
  apiKey: "AIzaSyBuIi2IUadkOJUk8Wk_uIazVENO6QroSvI",
  authDomain: "test-182ef.firebaseapp.com",
  databaseURL: "https://test-182ef.firebaseio.com",
  projectId: "test-182ef",
  storageBucket: "test-182ef.appspot.com",
  messagingSenderId: "838267606143"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  //console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
