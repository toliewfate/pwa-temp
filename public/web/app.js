var app = angular.module('template', ['firebase', 'ngRoute', 'template.home', 'template.gps', 'template.rtdata', 'template.noti', 'template.vibration', 'template.service', 'template.factory']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: './web/views/home.html',
        controller: 'HomeController',
    })
    .when('/gps', {
        templateUrl: './web/views/gps.html',
        controller: 'GpsController',
    })
    .when('/noti', {
        templateUrl: './web/views/noti.html',
        controller: 'NotiController',
    })
    .when('/vibration', {
        templateUrl: './web/views/vibration.html',
        controller: 'VibrationController',
    })
    .when('/rtdata', {
        templateUrl: './web/views/rtdata.html',
        controller: 'RtDataController',
    });
    // $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: false
    // });
}]);

app.run(function run() {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/sw.js')
        .then(function(reg) {
            // registration worked
            console.log('Registration succeeded. Scope is ', reg);
        }).catch(function(error) {
            // registration failed
            console.log('Registration failed with ' + error);
        });
    }
    var config = {
        apiKey: "AIzaSyBuIi2IUadkOJUk8Wk_uIazVENO6QroSvI",
        authDomain: "test-182ef.firebaseapp.com",
        databaseURL: "https://test-182ef.firebaseio.com",
        projectId: "test-182ef",
        storageBucket: "test-182ef.appspot.com",
        messagingSenderId: "838267606143"
    };
    firebase.initializeApp(config);
});


