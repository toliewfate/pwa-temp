var myApp = angular.module("template.gps", []);


myApp.controller('GpsController', function($scope, urlService, urlFactory, $interval, itemService) {
    // Parameter Start

    $scope.lat = "";
    $scope.lng = "";

    // Parameter End

    // Function Start
    $scope.init = function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(gps) {
                $scope.lat = gps.coords.latitude;
                $scope.lng = gps.coords.longitude;
            });
        }
    };
        
    // Function End

    // Directive Start
    $scope.header = {
        delegates: {
        },
        configs: {
        },
    };
    // Directive End
    $scope.init();
});