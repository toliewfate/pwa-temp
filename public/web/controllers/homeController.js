var myApp = angular.module("template.home", []);


myApp.controller('HomeController', function($scope, urlService, urlFactory, $interval, itemService) {
    // Parameter Start
    $scope.message = 0;
    // Parameter End

    // Function Start
    $scope.init = function() {
    };
    
    function  callAtInterval(){
        $scope.message++;
    }

    $scope.toast = function(){
        Materialize.toast('I am a toast!', 4000); // 4000 is the duration of the toast
    }
    $interval(callAtInterval, 1000, false);
    // Function End

    // Directive Start
    $scope.header = {
        delegates: {
        },
        configs: {
        },
    };
    // Directive End
    $scope.init();
});
