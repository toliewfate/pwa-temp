var myApp = angular.module("template.noti", []);


myApp.controller('NotiController', function($scope, urlService, urlFactory, $interval, itemService) {
    // Parameter Start
    const messaging = firebase.messaging();
    $scope.token = "";
    // Parameter End

    // Function Start
    $scope.init = function() {

    };

    messaging.requestPermission()
    .then(function(){
        return messaging.getToken();
    })
    .then(function(token){
        console.log(token);
        $scope.token = token;
    })
    .catch(function(err){
        console.log(err);
    });  
    
    // Function End

    // Directive Start
    $scope.header = {
        delegates: {
        },
        configs: {
        },
    };
    // Directive End
    $scope.init();
});
