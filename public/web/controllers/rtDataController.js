var myApp = angular.module("template.rtdata", []);


myApp.controller('RtDataController', function($scope, urlService, urlFactory, $interval, itemService) {
    // Parameter Start
    $scope.token = "";
    
    // Parameter End

    // Function Start
    $scope.init = function() {
        itemService.setListToScope($scope, "data");
    };
    // Function End

    // Directive Start
    $scope.header = {
        delegates: {
        },
        configs: {
        },
    };
    // Directive End
    $scope.init();
});
