var myApp = angular.module("template.vibration", []);


myApp.controller('VibrationController', function($scope, urlService, urlFactory, $interval, itemService) {
    // Parameter Start
    // Parameter End

    // Function Start
    $scope.init = function() {
    };

    $scope.alert = function(){
        if (window.navigator && window.navigator.vibrate) {
            alert('supported');
            window.navigator.vibrate(200);
        } else {
            alert('not supported');
        }
    };
    // Function End

    // Directive Start
    $scope.header = {
        delegates: {
        },
        configs: {
        },
    };
    // Directive End
    $scope.init();
});
