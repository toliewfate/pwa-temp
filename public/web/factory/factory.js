var myApp = angular.module("template.factory", []);

myApp.factory('urlFactory', function($location) {
  return {
    server: function() {
      return $location.protocol() + '://'+ $location.host()+'/iwin180/laravel/public/api/';
    },
  }
});

myApp.factory('noti', function($location) {
  return {
    start: function() {
      console.log("sss");
      const messaging = firebase.messaging();

      messaging.requestPermission()
      .then(function(){
          console.log("can");
          return messaging.getToken();
      })
      .catch(function(err){
          console.log(err);
      })

      messaging.onMessage(function(payload) {
        
          console.log("Message received. ", payload);
      });
    },
  }
});