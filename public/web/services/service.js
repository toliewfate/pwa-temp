var myApp = angular.module("template.service", []);

myApp.service('urlService', function($location) {
  return {
    server: function() {
      return $location.protocol() + '://'+ $location.host() + ':' + $location.port() + '/#!';
    },
  }
});

myApp.service('TemplateService', function($http, $q, $location, urlService) {
  return {
    post: function(obj) {
      var defer = $q.defer();
      var data = {
        email: obj.email,
  			password: obj.password,
  		};
  		var postURL = urlService.server()+"auth/authenticate";
  		$http.post(postURL,data).then(function(response) {
  			defer.resolve(response);
  		}).catch(function(response) {
  			defer.reject(response);
  		});
      return defer.promise;
    },
    get: function(obj) {
      var defer = $q.defer();
      var getURL = urlService.server();
      angular.forEach(obj, function(value , key) {
        searchMember += key+"="+value+"&";
      });
			$http.get(getURL).then(function(response) {
			    defer.resolve(response);
			}, function(response) {
			    defer.reject(response);
			});
			return defer.promise;
    },
  }
});


app.factory('itemService', function myService($firebaseObject) {
		return {
			setListToScope: function(scope, localScopeVarName) {
				var ref = firebase.database().ref().child("object");
        var syncObject = $firebaseObject(ref);
        syncObject.$bindTo(scope, localScopeVarName);
			},
		};
});